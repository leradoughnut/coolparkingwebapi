using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace CoolParking.WebAPI.Controllers  {
[Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase {
        private IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService){
            _parkingService = parkingService;
        }
        
        //POST api/vehicles   
        [HttpPost]
        public ActionResult<Vehicle> AddNewVehicle(JObject json){
            try{
                Vehicle newVehicle = JsonConvert.DeserializeObject<Vehicle>(json.ToString());
                _parkingService.AddVehicle(newVehicle);
                return Created(string.Format("api/parking"), newVehicle);
            }catch(ArgumentException){
                return BadRequest();
            }
        }
       /* public ActionResult<Vehicle> AddNewVehicle([Bind("Id", "VehicleType", "Balance")] Vehicle newVehicle) {
            try{ 
                if(ModelState.IsValid){
                     _parkingService.AddVehicle(newVehicle);
                    return Created(string.Format("api/parking"), newVehicle);
                }
                return BadRequest();
            
            }catch{
                return BadRequest();
            }
        } 
        */
        //GET api/vehicles
        [HttpGet]
        public ActionResult<int> Get() {
            return Ok(_parkingService.GetVehicles().Count);
        }      

        //GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id) {
            return Ok(_parkingService.GetVehicle(id));
        }

        //DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult<Vehicle> Delete(string id) {
            try{
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }catch(InvalidOperationException){
                return NotFound();
            }catch(ArgumentException){
                return BadRequest();
            }
        }
    }
}