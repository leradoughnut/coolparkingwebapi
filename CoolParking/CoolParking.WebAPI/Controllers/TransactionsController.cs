using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace CoolParking.WebAPI.Controllers  {
[Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase{
         private IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService){
            _parkingService = parkingService;
        }

        //GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<TransactionInfo> GetLastTransaction(){
            return Ok(_parkingService.GetLastParkingTransactions().LastOrDefault());
        }

        //GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<TransactionInfo[]> GetAllTransactions(){
            return Ok(_parkingService.GetLastParkingTransactions());
        } 

        //PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(JObject json){
            try{
                string id = json["id"].ToString();
                decimal sum = json["Sum"].ToObject<decimal>();
                return Ok(_parkingService.TopUpVehicle(id, sum));
            }catch(InvalidOperationException){
                return BadRequest();
            }catch(ArgumentException){
                return NotFound();
            }
        }

    }
}