using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace CoolParking.WebAPI.Controllers{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase {
        private IParkingService _parkingService;
        public ParkingController(IParkingService parkingService){
            _parkingService = parkingService;
        }
        
        //GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance(){
            return Ok(_parkingService.GetBalance());
        }

        //GET api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity(){
            return Ok(_parkingService.GetCapacity());
        }

        //GET api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces(){
            return Ok(_parkingService.GetFreePlaces());
        }

        

    }

}