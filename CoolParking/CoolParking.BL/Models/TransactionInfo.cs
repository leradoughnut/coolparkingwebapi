﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(string vehicleId, DateTime transactionDate, decimal sum)
        {
            VehicleId = vehicleId;
            TransactionDate = transactionDate;
            Sum = sum;
        }
        public string VehicleId { get; }
        public DateTime TransactionDate { get; }
        public decimal Sum { get; set; }
    }
}    