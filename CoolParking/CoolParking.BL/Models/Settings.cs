﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task


using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialBalance { get; set; } = 0;
        public static int IntervalForWithdrawMoney { get; set; } = 5000;
        public static int IntervalForLoggingToFile { get; set; } = 60000;
        public static int ParkingSpace { get; set; } = 10;
        public static decimal Fine { get; set; }= 2.5M;
        public static string LoggingFileName = "Transactions.log";

        public static ReadOnlyDictionary<VehicleType, decimal> PriceList = new ReadOnlyDictionary<VehicleType, decimal>
        (new Dictionary<VehicleType, decimal>(4)
            {
                { VehicleType.Bus, 3.5M },
                { VehicleType.Motorcycle, 1 },
                { VehicleType.Truck, 5 },
                { VehicleType.PassengerCar, 2 }
            });
    }
}