﻿//       The Balance should be able to change only in the CoolParking.BL project.

using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace CoolParking.BL.Models
{
    using Interfaces;
    using System.Linq;
    using System.Text;

    public class Vehicle : IVehicle
    {
        public decimal Balance { get; internal set; }
        public VehicleType VehicleType { get; }
        public string Id { get; }
        
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!IsValid(id) || (balance < 0)) throw new ArgumentException();
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private static bool IsValid(string id)
        {
            string strRegex = "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"; 
            Regex re = new Regex(strRegex);
            return (re.IsMatch(id));
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string vehicleId = "";
            string dictionaryDigits = "1234567890";
            string dictionaryLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random random = new Random();
            do
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(dictionaryLetters[random.Next(dictionaryLetters.Length)]);
                stringBuilder.Append(dictionaryLetters[random.Next(dictionaryLetters.Length)]);
                stringBuilder.Append("-");
                stringBuilder.Append(dictionaryDigits[random.Next(dictionaryDigits.Length)]);
                stringBuilder.Append(dictionaryDigits[random.Next(dictionaryDigits.Length)]);
                stringBuilder.Append(dictionaryDigits[random.Next(dictionaryDigits.Length)]);
                stringBuilder.Append(dictionaryDigits[random.Next(dictionaryDigits.Length)]);
                stringBuilder.Append("-");
                stringBuilder.Append(dictionaryLetters[random.Next(dictionaryLetters.Length)]);
                stringBuilder.Append(dictionaryLetters[random.Next(dictionaryLetters.Length)]);
                vehicleId = stringBuilder.ToString();
            } while (Parking.Instance.Vehicles.Any(v => v.Id == vehicleId));
            return vehicleId;
        }
    }
}