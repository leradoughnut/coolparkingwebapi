namespace CoolParking.BL.Interfaces
{
    using CoolParking.BL.Models;
    public interface IVehicle
    {
        string Id { get; }
        decimal Balance { get; }
        VehicleType VehicleType { get; }
    }
}